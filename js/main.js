greetButton = document.getElementsByClassName('btn')[0];
greetButton.id = "add";

let inputDiam = document.createElement('input');
let inputColor = document.createElement('input');
let inputVal = inputDiam.value;
let inputValCol = inputColor.value;
let createBtn = document.createElement('btn');

document.addEventListener('click',(e)=>{
    inputDiam.placeholder = "Введите диаметр круга?";
    inputDiam.style = ("width:200px;height:20px;margin-right:15px");
    inputColor.placeholder = "Цвет круга?";
    inputColor.style = ("width:150px;height:20px;margin-right:15px");
    createBtn.innerText = "Нарисовать";
    createBtn.classList.add('btn');
    createBtn.id = "create";
    
    if(e.target.id === "add") {
        greetButton.style.display="none";
        document.body.appendChild(inputDiam);
        document.body.appendChild(inputColor);
        document.body.appendChild(createBtn);  
    }
     if (e.target.id === "create") {
         let simpleDiv = document.createElement('div');
         document.body.appendChild(simpleDiv);
         let circle = document.createElement('div');
         circle.class = "circle";

         circle.style.backgroundColor = inputColor.value;
         circle.style.width = inputDiam.value + "px";
         circle.style.height = inputDiam.value + "px";
         circle.style.borderRadius = "50%";
         simpleDiv.appendChild(circle);
     }
});

